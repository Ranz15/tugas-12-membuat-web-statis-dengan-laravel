<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Get Method

// Menampilkan halaman home
Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@index');

// Post Method
Route::post('/welcome', 'AuthController@welcome');