<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('register');
    }

    public function welcome(Request $request){
        // dd($request);
        $first_name = $request['nama_depan'];
        $last_name = $request['nama_belakang'];

        return view('welcome', compact('first_name', 'last_name'));
    }

}
