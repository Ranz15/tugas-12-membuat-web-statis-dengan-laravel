<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        {{-- Function keamanan milik laravel --}}
        @csrf

        <!-- First Name Input -->
        <label for="FN"> First Name: </label> <br /><br />
        <input type="text" name="nama_depan" id="FN" /> <br /><br />

        <!-- Last Name Input -->
        <label for="LN"> Last Name: </label> <br /><br />
        <input type="text" name="nama_belakang" id="LN" /> <br /><br />

        <!-- Gender Input -->
        <label for="GN"> Gender: </label> <br /><br />
        <input type="radio" name="Jenis_Kelamin" value="male"/> Male <br />
        <input type="radio" name="Jenis_Kelamin" value="female"/> Female <br />
        <input type="radio" name="Jenis_Kelamin" value="other"/> Other <br /><br />

        <!-- Nationality Input -->
        <label for="NT"> Nationality: </label> <br /><br />
        <select name="warna_negara" id="">
            <option value="IDN">Indonesian</option>
            <option value="ZMB">Zimbabwe</option>
            <option value="Thl">Thailand</option>
            <option value="RSN">Russian</option>
        </select>
        <br /><br />

        <!-- Language Input -->
        <label for="LGG"> Language Spoken: </label> <br /><br />
        <input type="checkbox" name="LGG" value="IDN"/>Bahasa Indonesia <br />
        <input type="checkbox" name="LGG" value="EN"/>English <br />
        <input type="checkbox" name="LGG" value="Unknown"/>Other <br /><br />

        <!-- Bio Input -->
        <label for="Bio"> Bio: </label> <br /><br />
        <textarea name="bio" id="Bio" cols="25" rows="10"></textarea> <br /><br />

        <!-- Save Button -->
        <input type="submit" value="Sign Up" />
    </form>
</body>
</html>